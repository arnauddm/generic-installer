#!/bin/bash

if [ -d "build/" ]
then
    rm -rf build
fi

TRUE=1
FALSE=0
EMPTY= 

CMAKE_INSTALL_DIR=.
COMPILATION_MODE=-DCMAKE_BUILD_TYPE=Release
PREFIXPATH=$EMPTY

for arg in "$@"
do
    case "$arg" in
        "--debug")
            COMPILATION_MODE=-DCMAKE_BUILD_TYPE=Debug
            ;;
        *)
            echo "./configure.sh --(debug)"
            exit 1
            ;;
    esac
done

if [ $(uname) == "Darwin" ]
then

	if [ -f $(command -v brew) ]
	then
	    QT5_PREFIX=$(brew --prefix qt5)
        JSONCPP_PREFIX=$(brew --prefix jsoncpp)/include
	else
	    echo "Can't determine where dependencies are installed..."
	    echo "Aborting..."
	    exit 1
	fi
	PREFIXPATH="-DCMAKE_PREFIX_PATH=$QT5_PREFIX;$JSONCPP_PREFIX"
fi

mkdir build
cd build
cmake .. $COMPILATION_MODE $PREFIXPATH
