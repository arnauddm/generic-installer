#ifndef __CONFIGURATION_ITEM_HPP__
#define __CONFIGURATION_ITEM_HPP__

#include <QObject>
#include <QString>

class ConfigurationItem : public QObject
{
    Q_OBJECT
public:
    ConfigurationItem(void);
    ConfigurationItem(const QString & title, const QString & description, const QString & command);
    ~ConfigurationItem(void);

    Q_INVOKABLE QString title(void);
    Q_INVOKABLE QString description(void);
    Q_INVOKABLE QString command(void);

    Q_INVOKABLE void setState(bool checked);

    bool isChecked(void);

private:

    QString _title;
    QString _description;
    QString _command;
    bool _isChecked;
};

#endif