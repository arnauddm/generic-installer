#include "installer.hpp"
#include "configurationParser.hpp"
#include <QQmlContext>
#include <QProcess>
#include <QThread>
#include <iostream>

Installer::Installer(void) : _engine(nullptr), _configuration(new Configuration())
{
}

Installer::~Installer(void)
{
    if(_configuration)  delete _configuration;
}

void Installer::setEngine(QQmlApplicationEngine * engine)
{
    _engine = engine;
}

QString Installer::configurationFile(void)
{
    return _configurationPath;
}

void Installer::setConfigurationFile(const QUrl & file)
{
    _configurationPath = QUrl(file).toLocalFile();
    parseConfiguration();
}

void Installer::parseConfiguration(void)
{       
    _configuration  = ConfigurationParser::parse(_configuration, _configurationPath);

    emit configurationUpdated();
}

Configuration * Installer::configuration(void)
{
    return _configuration;
}

void Installer::process(void)
{
    int totalItems = 10; //configuration()->count();
    
    for(int i = 0; i < totalItems; i++)
    {
        ConfigurationItem * item = configuration()->get(i);
        bool finished = true;
        if(item->isChecked())
        {
            std::cout << "Processing : " << item->command().toStdString() << std::endl;
            QProcess process;
            process.start(item->command());
            finished = process.waitForFinished(10*60*1000); // Timeout : 10min
        }

        emit updateProgressBar(QVariant::fromValue(finished ? i+1 : -1), QVariant::fromValue(totalItems), QVariant::fromValue(item->title()));
    }
}