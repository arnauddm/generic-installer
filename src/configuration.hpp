#ifndef __CONFIGURATION_HPP__
#define __CONFIGURATION_HPP__

#include <QVector>
#include <QObject>
#include "configurationItem.hpp"

class Configuration : public QObject
{
    Q_OBJECT
public:
    Configuration(void);
    ~Configuration(void);

    void append(const QString & title, const QString & description, const QString &command);

    Q_INVOKABLE int count(void);
    Q_INVOKABLE ConfigurationItem * get(int index);

private:
    QVector<ConfigurationItem *> _items;
};

#endif