#include "configurationItem.hpp"

ConfigurationItem::ConfigurationItem(void)
{

}

ConfigurationItem::ConfigurationItem(const QString & title, const QString & description, const QString & command) :
    _title(title),
    _description(description),
    _command(command),
    _isChecked(false)
{

}

ConfigurationItem::~ConfigurationItem(void)
{

}

QString ConfigurationItem::title(void)
{
    return _title;
}

QString ConfigurationItem::description(void)
{
    return _description;
}

QString ConfigurationItem::command(void)
{
    return _command;
}

void ConfigurationItem::setState(bool checked)
{
    _isChecked = checked;
}

bool ConfigurationItem::isChecked(void)
{
    return _isChecked;
}