import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.15
import QtQuick.Controls.Styles 1.4

import "../components/."

RowLayout {
    anchors.fill: parent
    objectName: "installationView"

    function onUpdateProgressBar(value, max, text) {
        processingStatus.indeterminate = false
        processingStatus.to = max
        processingStatus.value = value
        processingText.text = text

        if(value == max)
        {
            ctrlBtns.enableNext = false
            popupmessage.title = "Almost done"
            popupmessage.content = "All selected operations are processed !"
            popupmessage.openPopup()
        } else if(value == -1)
        {
            ctrlBtns.enableNext = false
            popupmessage.title = "An error occured..."
            popupmessage.content = "An error occured while processing " + text
            popupmessage.openPopup()
        }
    }

    SectionTitle {
        title: "Processing"
    }

    SectionContent {  
        ColumnLayout {
            anchors.centerIn: parent
            spacing: 20

            ProgressBar {        
                id: processingStatus
                indeterminate: true
                implicitWidth: parent.parent.width
            }

            Text {
                id: processingText
                text: "Processing..."
                horizontalAlignment: Text.AlignHCenter
            }

            ControlButtons {
                id: ctrlBtns
                enableNext: true
                textNext: "GO !"
            }
        }
    }

    PopupMessage {
        id: popupmessage
    }
}