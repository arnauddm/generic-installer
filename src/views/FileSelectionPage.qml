import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.15

import Installer 1.0

import "../components/."

RowLayout {
    
    anchors.fill: parent

    SectionTitle {
        title: "GENERIC INSTALLER"
    }

    SectionContent {

        ColumnLayout {
            
            anchors.fill: parent
            
            Button {
                id: filebutton
                text: qsTr("CHOOSE FILE")
                spacing: 2
                padding: 10
                Layout.fillWidth: true
                anchors.centerIn: parent
                onClicked: {
                    fileDialog.open()
                }
                implicitWidth: parent.width * 0.6
                Material.background: "#ffffff"
                Material.foreground: Material.accent
                Accessible.description: qsTr("Select this button to change the configuration file")
                Accessible.onPressAction: clicked()
            }

            ControlButtons {
                id: controlButtons
                enableNext: false
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            installer.setConfigurationFile(fileDialog.fileUrl);
            filebutton.text = installer.configurationFile();
            controlButtons.enableNext = true
        }
        Component.onCompleted: visible = false
    }
}