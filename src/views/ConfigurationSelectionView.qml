import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.15
import Installer 1.0

import "../components/."


RowLayout {
    objectName: "configurationSelectionView"

    function onConfigurationUpdated() {
        var configuration = installer.configuration();
        for(let index = 0; index < configuration.count(); index++)
        {
            var confItem = configuration.get(index);
            configurationModel.append({
                "item": confItem
            });
        }
    }

    anchors.fill: parent
    SectionTitle {
        title: "Item selection"
    }

    SectionContent {
        ColumnLayout {
            anchors.fill: parent
            anchors.topMargin: 25
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            Layout.fillWidth: true
            

            Component {
                id: listdelegate
                
                RowLayout {
                    implicitWidth: parent.width
                    width: parent.width;

                    CheckBox {
                        id: check
                        checked: false
                        enabled: true

                        onClicked: {
                            item.setState(check.checked)
                        }
                    }

                    Rectangle {
                        x: 50
                                        
                        Column {
                            Text { text: '<b>' + item.title() + '</b>' }
                            Text { text: '<i>' + item.description() + '</i>' }
                        }
                    }
                }
            }

            ListView {
                id: listview           
                delegate: listdelegate
                model: configurationModel
                implicitHeight: parent.height * .9
                
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds
                ScrollBar.vertical: ScrollBar {
                    active: true
                    hoverEnabled: true
                }
            }
            

            ControlButtons {}
        }
    }

    ListModel {
        id: configurationModel
    }
}