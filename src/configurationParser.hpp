#ifndef __CONFIGURATION_PARSER_HPP__
#define __CONFIGURATION_PARSER_HPP__

#include "configuration.hpp"

class ConfigurationParser
{
public:
    static Configuration * parse(Configuration * conf, const QString & path);

private:
    explicit ConfigurationParser(void);
    virtual ~ConfigurationParser(void);
};

#endif