#include "configuration.hpp"

Configuration::Configuration(void)
{
    
}

Configuration::~Configuration(void)
{
}
    
void Configuration::append(const QString & title, const QString & description, const QString &command)
{
    _items.push_back(new ConfigurationItem(title, description, command));
}

int Configuration::count(void)
{
    return _items.size();
}

ConfigurationItem * Configuration::get(int index)
{
    return _items.at(index);
}