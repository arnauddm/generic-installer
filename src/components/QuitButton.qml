import QtQuick.Controls 2.15
import QtQuick 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

Button {
    id: quitButton
    text: qsTr("X")
    spacing: 0
    padding: 0
    bottomPadding: 0
    topPadding: 0
    enabled: true
    Layout.minimumHeight: 40
    Layout.fillWidth: true

    onClicked: {
        Qt.quit()
    }
    
    Material.background: "#ffffff"
    Material.foreground: "#c51a4a"
    Accessible.description: qsTr("Select this button to close the application")
    Accessible.onPressAction: clicked()
}