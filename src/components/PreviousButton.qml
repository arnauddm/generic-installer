import QtQuick.Controls 2.15
import QtQuick 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

Button {
    id: previousButton
    text: qsTr("Previous")
    spacing: 0
    padding: 0
    bottomPadding: 0
    topPadding: 0
    Layout.minimumHeight: 40
    Layout.fillWidth: true

    onClicked: {
        view.currentIndex = view.currentIndex - 1
    }
    
    Material.background: "#ffffff"
    Material.foreground: Material.Accent
    Accessible.description: qsTr("Select this button to go to the previous page")
    Accessible.onPressAction: clicked()
}