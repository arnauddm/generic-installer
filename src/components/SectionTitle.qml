import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.15

Rectangle {
    
    property alias title: title.text
    
    implicitHeight: parent.height - 30
    implicitWidth: (parent.width * 0.5) - 30
    color: Material.accent
    radius: 20
    Layout.margins: 15
    Layout.rightMargin: 15 / 2

    Text {
        id: title
        color: "#ffffff"
        font.pointSize: 24
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
    }
}