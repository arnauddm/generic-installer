import QtQuick.Controls 2.15
import QtQuick 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

Popup {
    id: popup

    property alias title: title.text
    property alias content: content.text

    x: (parent.width * 0.6) / 2
    implicitWidth: parent.width * 0.4

    y: (parent.height * 0.4) / 2
    implicitHeight: parent.height * 0.6

    ColumnLayout {
        anchors.fill: parent
        
        Rectangle {        
            implicitHeight: parent.height * 0.3
            implicitWidth: parent.width
            color: Material.accent
            x: 0
            y: 0
            Layout.margins: 0

            Text {
                id: title
                color: "white"
                font.pointSize: 18
                horizontalAlignment: Text.AlignHCenter
                anchors.centerIn: parent
            }
        }

        Rectangle {        
            implicitHeight: parent.height * 0.5
            implicitWidth: parent.width
            color: "#fff"

            Text {
                id: content
                color: "#000"
                font.pointSize: 12
                horizontalAlignment: Text.AlignHCenter
                anchors.centerIn: parent
            }
        }

        Button {
            id: closeBtn
            text: "Close"

            y: (parent.width - closeBtn.width) / 2

            onClicked: {
                close()
            }
        }
    }

    function openPopup() {
        open()
        // trigger screen reader to speak out message
        popup.forceActiveFocus()
    }
}