import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.15


Rectangle {
    id: rect
    Layout.fillWidth: true
    color: Material.accent

    Text {
        text: "GENERIC INSTALLER"
        color: "white"
        Layout.fillWidth: true
        Layout.preferredHeight: 17
        Layout.preferredWidth: 100
        font.pixelSize: 24
        font.bold: false
        horizontalAlignment: Text.AlignHCenter    
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        
    }
}