import QtQuick.Controls 2.15
import QtQuick 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

Button {
    id: nextButton
    text: qsTr("Next")
    spacing: 0
    padding: 0
    bottomPadding: 0
    topPadding: 0
    Layout.minimumHeight: 40
    Layout.fillWidth: true    

    onClicked: {
        view.currentIndex = view.currentIndex + 1
        if(view.currentIndex == view.count)
        {
            installer.process()
        }
    }
    
    Material.background: Material.accent
    Material.foreground: "#ffffff"
    Accessible.description: qsTr("Select this button to go to the next page")
    Accessible.onPressAction: clicked()
}