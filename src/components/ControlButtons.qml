import QtQuick.Controls 2.15
import QtQuick 2.15
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

RowLayout {
    property alias enablePrevious: previous.enabled
    property alias enableNext: next.enabled

    property alias textPrevious: previous.text
    property alias textNext: next.text

    spacing: 4
    Layout.bottomMargin: 0
    Layout.rightMargin: 0

    PreviousButton {
        id: previous
        enabled: view.currentIndex != 0
    }

    NextButton {
        id: next
        enabled: view.currentIndex != view.count - 1
    }

}