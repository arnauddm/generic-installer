import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.15

Rectangle { 
    
    implicitHeight: parent.height - 30
    implicitWidth: parent.width * 0.5 - 30

    Layout.margins: 15
    Layout.leftMargin: 15 / 2
    radius: 20

    color: "#e0e2e1"

}