import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.15


Text {
    color: Material.accent
    Layout.fillWidth: true
    Layout.preferredHeight: 17
    Layout.preferredWidth: 100
    font.pixelSize: 20
    font.bold: false
    horizontalAlignment: Text.AlignHCenter    
}