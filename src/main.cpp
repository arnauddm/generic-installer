#include <QFileInfo>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTextStream>
#include <QMessageLogContext>
#include <QQuickWindow>
#include <QQuickView>
#include <QtQuick/QQuickView>
#include <QGuiApplication>

#include "installer.hpp"
#include "configuration.hpp"
#include "configurationItem.hpp"

#include <QtQml/QtQml>

inline QObject * getElement(QQmlApplicationEngine * engine, const QString & elementName);

int main(int argc, char *argv[])
{

    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    app.setOrganizationName("Arnaud DE MATTEIS");
    app.setOrganizationDomain("arnauddematteis.fr");
    app.setApplicationName("Generic Installer");
    //app.setWindowIcon(QIcon(":/icons/rpi-imager.ico"));

    qmlRegisterType<Installer>("Installer", 1,0, "Installer");
    qmlRegisterType<Configuration>("Configuration", 1, 0, "Configuration");
    qmlRegisterType<ConfigurationItem>("ConfigurationItem", 1, 0, "ConfigurationItem");
    
    QQmlApplicationEngine engine;

    Installer installer;
    installer.setEngine(&engine);

    engine.rootContext()->setContextProperty("installer", &installer);

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    if (engine.rootObjects().isEmpty())
        return -1;

    QObject *qmlwindow = engine.rootObjects().value(0);

    qmlwindow->connect(&installer, SIGNAL(configurationUpdated()), getElement(&engine, "configurationSelectionView"), SLOT(onConfigurationUpdated()));
    qmlwindow->connect(&installer, SIGNAL(updateProgressBar(QVariant, QVariant, QVariant)), getElement(&engine, "installationView"), SLOT(onUpdateProgressBar(QVariant, QVariant, QVariant)));


    return app.exec();
}

inline QObject * getElement(QQmlApplicationEngine * engine, const QString & elementName)
{
    return engine->rootObjects().value(0)->findChild<QObject *>(elementName);
}