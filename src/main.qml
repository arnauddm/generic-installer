import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Layouts 1.11
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.15

import Installer 1.0
import Configuration 1.0

import "views/."
import "components/."

ApplicationWindow {
    id: window
    visible: true

    width: 800
    height: 600

    title: "Generic Installer"

    Material.theme: Material.Light
    Material.accent: "#FBB040"

    color: "#ffffff"   

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        Layout.topMargin: 0

        SwipeView {
            id: view
            
            anchors.fill: parent
            
            currentIndex: 0
            interactive: false

            Item {
                FileSelectionPage {}
            }

            Item {
                ConfigurationSelectionView {}
            }

            Item {
                InstallationView {}
            }
        }

    }

    footer: PageIndicator {
        currentIndex: view.currentIndex
        count: view.count
        anchors.centerIn: parent
    }
}