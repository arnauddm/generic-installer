#ifndef __INSTALLER_HPP__
#define __INSTALLER_HPP__

#include <QObject>
#include <QQmlApplicationEngine>

#include "configuration.hpp"

class Installer : public QObject
{
    Q_OBJECT

public:
    explicit Installer(void);
    virtual ~Installer(void);
    
    void setEngine(QQmlApplicationEngine * engine);


    Q_INVOKABLE QString configurationFile(void);
    Q_INVOKABLE void setConfigurationFile(const QUrl & file);

    Q_INVOKABLE void parseConfiguration(void);

    Q_INVOKABLE Configuration * configuration(void);

    Q_INVOKABLE void process(void);

signals:
    void configurationUpdated();
    void updateProgressBar(QVariant value, QVariant max, QVariant text);

private:
    QQmlApplicationEngine * _engine;

    Configuration * _configuration;
    QString _configurationPath;
};

#endif