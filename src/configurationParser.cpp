#include "configurationParser.hpp"
#include <json/reader.h>
#include <QFile>
#include <QUrl>

#include <iostream>

ConfigurationParser::ConfigurationParser(void)
{

}

ConfigurationParser::~ConfigurationParser(void)
{

}

Configuration * ConfigurationParser::parse(Configuration * conf, const QString & path)
{
    QFile file(path);
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    std::stringstream stream;
    stream << file.readAll().toStdString();

    Json::Value root;

    std::string errors;
    Json::CharReaderBuilder builder;

    if (!Json::parseFromStream(builder, stream, &root, &errors)) {
        return conf;
    }

    const Json::Value jsonFiles = root["entries"];
    for (unsigned int i = 0; i < jsonFiles.size(); i++)
    {
        Json::Value jsonFile = jsonFiles[i];

        
        QString title = QString::fromStdString(jsonFile.get("title", "").asString());
        QString description = QString::fromStdString(jsonFile.get("description", "").asString());
        QString command = QString::fromStdString(jsonFile.get("command", "").asString());

        conf->append(title, description, command);

    }

    file.close();

    return conf;

}