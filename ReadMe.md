# Docarchiver

This project is enhanced by a personal issue : create an easy to use graphic application to easily select and run bahs commands?

# Screenshot example

<img src="docs/home.png" width="50%"/><img src="docs/selection.png" width="50%"/>
<img src="docs/installation.png" width="50%"/>



## Getting Started

### Prerequisites

Following packages are needed :

* `cmake`
* `gcc` or `g++` or `ninja`
* `Qt5 Core`
* `Qt5 Widgets`
* `Qt5 Quick`
* `jsoncpp`

### Installing

Before building the software, you need to generate the Makefile with CMake.
To do this, the [`configure.sh`](configure.sh) allows you to interact easily with CMake :

```shell
$ ./configure.sh
```

This script can take following options :
* `--debug` : enable debug mode

Then, build and run binaries :
```shell
$ cd build
$ make
$ ./src/generic-installer
```


## Documentation

Empty section for the moment

## Authors

  - [**Arnaud DE MATTEIS**](https://gitlab.com/arnauddm)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. This section will be update further to add more details about the procedure to follow.

## License

This project is licensed under the [GNU GPLv3](LICENSE.md)
Creative Commons License - see the [LICENSE.md](LICENSE.md) file for
details
